#include <iostream>
#include "AssertUtils.h"

using namespace std;

int AssertUtils::countError = 0;
int AssertUtils::countSuccess = 0;

void AssertUtils::assertTrue(string programName, string msg, bool value) {
    if (value != true) {
        cerr << programName << ": " << msg << endl;
        AssertUtils::incErrorCount();
    } else {
        AssertUtils::incSuccessCount();
    }
}

void AssertUtils::clear() {
    AssertUtils::countError = 0;
    AssertUtils::countSuccess = 0;
}


int AssertUtils::incErrorCount() {
    return ++countError;
}

int AssertUtils::incSuccessCount() {
    return ++countSuccess;
}

void AssertUtils::summarize() {
    if (countError == 0) {
        cout << "Successful Tests: " << countSuccess << " Failed Tests: 0" << endl;
    } else {
        cerr << "Successful Tests: " << countSuccess << " Failed Tests: 0" << endl;
    }
}