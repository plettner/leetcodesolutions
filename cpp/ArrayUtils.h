#ifndef ARRAY_UTILS_H
#define ARRAY_UTILS_H

#include <vector>

class ArrayUtils {

public :
    void print(int * pArray, int length);
    bool areEqual(int * pLeft, int * pRight, int length);
    bool areEqual(std::vector<int> * pLeft, std::vector<int> * pRight);
};

#endif