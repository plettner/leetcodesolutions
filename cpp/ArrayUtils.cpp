#include <iostream>
#include <vector>

#include "ArrayUtils.h"

using namespace std;

void ArrayUtils::print(int * pArray, int length) {
    std::cout << "[";
    for (int index = 0; index < length; index++) {
        if (index > 0) {
            cout << ", ";
        }
        cout << pArray[index];
    }
    std::cout << "]" << endl;
}

/**
 * Assumes the arrays are the same length.  Returns true
 * if the arrays have the same values for index positions
 * 0 through length - 1.  Returns false if any values don't
 * match.
 */
bool ArrayUtils::areEqual(int * pLeft, int * pRight, int length) {
    for (int index = 0; index < length; index++) {
        if (pLeft[index] != pRight[index]) {
            return false;
        }
    }

    return true;
}

bool ArrayUtils::areEqual(vector<int> * pLeft, vector<int> * pRight) {
    
    if (pLeft == pRight) {
        return true;
    }

    if (pLeft == NULL || pRight == NULL) {
        return false;
    }

    if (pLeft->size() != pRight->size()) {
        return false;
    }

    for (int index = 0; index < pLeft->size(); index++) {
        if ((*pLeft)[index] != (*pRight)[index]) {
            return false;
        }
    }

    return true;
}
