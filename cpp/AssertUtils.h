#ifndef ASSERT_UTILS_H
#define ASSERT_UTILS_H

#include <string>

class AssertUtils {

private :
    static int countError;
    static int countSuccess;

public :
    static void assertTrue(std::string program, std::string msg, bool value);

    static void clear();
    static void summarize();
    static int incErrorCount();
    static int incSuccessCount();

};

#endif
