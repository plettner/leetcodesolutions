#include <stdlib.h>
#include <iostream>
#include <vector>

#include "PlusOne.h"
#include "ArrayUtils.h"
#include "AssertUtils.h"

using namespace std;

int * PlusOne::go(int digits[], int length) {
    if (length <= 0 || digits == NULL) {
        return NULL;
    }

    digits[length - 1] ++;

    return digits;
}

vector<int> * PlusOne::go(vector<int> * v) {
    if (v == NULL || v->size() == 0) {
        return NULL;
    }

    (*v)[v->size() - 1 ] =  v->at(v->size() - 1) + 1;
    return v;
}


int main(int argc, char ** argv) {

    ArrayUtils arrayUtils;
    atexit(AssertUtils::summarize);

    const int LENGTH1 = 3;
    int digits1[LENGTH1] = { 1, 2, 3 };
    int expected1 [LENGTH1] = { 1, 2, 4 };

    PlusOne plusOne;

    int * pActual = plusOne.go(digits1, LENGTH1);

    // arrayUtils.print(pActual, LENGTH1);

    bool rc = arrayUtils.areEqual(expected1, pActual, LENGTH1);
    AssertUtils::assertTrue(argv[0], "Arrays are not equal", rc);

    const int LENGTH2 = 4;
    int digits2[LENGTH2] = { 4, 3, 2, 1 };
    int expected2[LENGTH2] = { 4, 3, 2, 2 };
    pActual = plusOne.go(digits2, LENGTH2);
    rc = arrayUtils.areEqual(expected2, pActual, LENGTH2);
    AssertUtils::assertTrue(argv[0], "Arrays are not equal", rc);

    vector<int> v = { 1, 2, 3 };
    vector<int> expected = { 1, 2, 4 };
    vector<int> * pOutput = plusOne.go(&v);
    rc = arrayUtils.areEqual(&expected, pOutput);
    AssertUtils::assertTrue(argv[0], "Vectors don't match", rc);


}