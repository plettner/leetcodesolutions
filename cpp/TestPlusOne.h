#ifndef TEST_PLUS_ONE_H
#define TEST_PLUS_ONE_H

#include "PlusOne.h"

#include "/home/marvin/src/cpp/cppunit/include/cppunit/TestCase.h"
#include "/home/marvin/src/cpp/cppunit/include/cppunit/TestSuite.h"
#include "/home/marvin/src/cpp/cppunit/include/cppunit/TestCaller.h"

class TestPlusOne : public CppUnit::TestCase {

public :

    void testPlusOneSimple();

    // method to create a suite of tests
    static Test *suite ();

};

#endif
