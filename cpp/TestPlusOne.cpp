#include <iostream>
#include <string>
#include <assert.h>

#include "TestPlusOne.h"

using namespace std;

void TestPlusOne::testPlusOneSimple() {

    const int ARRAY_LENGTH = 10;
    int array[ARRAY_LENGTH];

    PlusOne plusOne;
    int * actual = plusOne.go(array, ARRAY_LENGTH);
    assert(actual != NULL);

}
