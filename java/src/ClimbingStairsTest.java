import org.junit.Assert;
import org.junit.jupiter.api.Test;

class ClimbingStairsTest {

	private final ClimbingStairs climbingStairs = new ClimbingStairs();

	@Test
	void example1() {
		final int value = 2;
		final int expected = 2;

		final int actual = this.climbingStairs.climbStairs(value);

		Assert.assertEquals(expected, actual);
	}

	/**
	 * Explanation: There are three ways to climb to the top:
	 * <ol>
	 * <li>1 step + 1 step + 1 step</li> 1 step + 2
	 * <li>1 step + 2 steps</li>
	 * <li>2 steps + 1 step</li>
	 * </ol>
	 */
	@Test
	void example2() {
		final int value = 3;
		final int expected = 3;

		final int actual = this.climbingStairs.climbStairs(value);

		Assert.assertEquals(expected, actual);
	}

	/**
	 * Explanation: There are 5 ways to climb to the top:
	 * <ol>
	 * <li>1 step + 1 step + 1 step + 1 step</li>
	 * <li>2 step + 1 step + 1 step</li>
	 * <li>1 step + 2 step + 1 step</li>
	 * <li>1 step + 1 step + 2 step</li>
	 * <li>2 step + 2 step</li>
	 * </ol>
	 */
	@Test
	void steps4() {
		final int value = 4;
		final int expected = 5;

		final int actual = this.climbingStairs.climbStairs(value);

		Assert.assertEquals(expected, actual);
	}

	/**
	 * Explanation: On five steps, here are X ways to climb to the top:
	 * <ol>
	 * <li>1 step + 1 step + 1 step + 1 step + 1 step</li>
	 * <li>2 step + 1 step + 1 step + 1 step</li>
	 * <li>1 step + 2 step + 1 step + 1 step</li>
	 * <li>1 step + 1 step + 2 step + 1 step</li>
	 * <li>1 step + 1 step + 1 step + 2 step</li>
	 * <li>2 step + 2 step + 1 step</li>
	 * <li>2 step + 1 step + 2 step</li>
	 * <li>1 step + 2 step + 2 step</li>
	 * </ol>
	 */
	@Test
	void steps5() {
		final int value = 5;
		final int expected = 8;

		final int actual = this.climbingStairs.climbStairs(value);

		Assert.assertEquals(expected, actual);
	}

	/**
	 * Explanation: On five steps, here are X ways to climb to the top:
	 * <ol>
	 * <li>1 step + 1 step + 1 step + 1 step + 1 step + 1 step</li>
	 * <li>2 step + 1 step + 1 step + 1 step + 1 step</li>
	 * <li>1 step + 2 step + 1 step + 1 step + 1 step</li>
	 * <li>1 step + 1 step + 2 step + 1 step + 1 step</li>
	 * <li>1 step + 1 step + 1 step + 2 step + 1 step</li>
	 * <li>1 step + 1 step + 1 step + 1 step + 2 step</li>
	 * <li>2 step + 2 step + 1 step + 1 step</li>
	 * <li>2 step + 1 step + 2 step + 1 step</li>
	 * <li>2 step + 1 step + 1 step + 2 step</li>
	 * <li>1 step + 2 step + 2 step + 1 step</li>
	 * <li>1 step + 2 step + 1 step + 2 step</li>
	 * <li>1 step + 1 step + 2 step + 2 step</li>
	 * <li>2 step + 2 step + 2 step</li>
	 * </ol>
	 */
	@Test
	void steps6() {
		final int value = 6;
		final int expected = 13;

		final int actual = this.climbingStairs.climbStairs(value);

		Assert.assertEquals(expected, actual);
	}

}
