import org.junit.Assert;
import org.junit.jupiter.api.Test;

import utils.ArrayUtils;

class MoveZeroesTest {

	private final MoveZeroes mz = new MoveZeroes();

	@Test
	void example1() {
		final int[] actual = { 0, 1, 0, 3, 12 };
		final int[] expected = { 1, 3, 12, 0, 0 };

		this.mz.moveZeroes(actual);

		Assert.assertTrue(ArrayUtils.equals(expected, actual));
	}

	@Test
	void example2() {
		final int[] actual = { 0 };
		final int[] expected = { 0 };

		this.mz.moveZeroes(actual);

		Assert.assertTrue(ArrayUtils.equals(expected, actual));
	}

	@Test
	void manyLeadingNonZeroes() {
		final int[] actual = { 1, 2, 3, 0, 12 };
		final int[] expected = { 1, 2, 3, 12, 0 };

		this.mz.moveZeroes(actual);

		Assert.assertTrue(ArrayUtils.equals(expected, actual));
	}

}
