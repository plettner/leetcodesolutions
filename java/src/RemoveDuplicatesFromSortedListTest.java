import org.junit.Assert;
import org.junit.jupiter.api.Test;

import utils.ListNode;

class RemoveDuplicatesFromSortedListTest {

	private final RemoveDuplicatesFromSortedList cut = new RemoveDuplicatesFromSortedList();

	@Test
	void example1() {
		final int[] array = { 1, 1, 2 };
		final int[] earray = { 1, 2 };
		final ListNode listNode = ListNode.fromArray(array);
		final ListNode expected = ListNode.fromArray(earray);

		final ListNode actual = this.cut.deleteDuplicates(listNode);

		Assert.assertTrue(ListNode.equals(expected, actual));
	}

	@Test
	void example2() {
		final int[] array = { 1, 1, 2, 3, 3 };
		final int[] earray = { 1, 2, 3 };
		final ListNode listNode = ListNode.fromArray(array);
		final ListNode expected = ListNode.fromArray(earray);

		final ListNode actual = this.cut.deleteDuplicates(listNode);

		Assert.assertTrue(ListNode.equals(expected, actual));
	}

	@Test
	void allDupes() {
		final int[] array = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
		final int[] earray = { 1 };
		final ListNode listNode = ListNode.fromArray(array);
		final ListNode expected = ListNode.fromArray(earray);

		final ListNode actual = this.cut.deleteDuplicates(listNode);

		Assert.assertTrue(ListNode.equals(expected, actual));
	}

	@Test
	void singleElement() {
		final int[] array = { 1 };
		final int[] earray = { 1 };
		final ListNode listNode = ListNode.fromArray(array);
		final ListNode expected = ListNode.fromArray(earray);

		final ListNode actual = this.cut.deleteDuplicates(listNode);

		Assert.assertTrue(ListNode.equals(expected, actual));
	}

}
