
public class RemoveDuplicatesFromSortedArray {

	public int removeDuplicates(int[] nums) {

		int index = nums.length - 1;
		int newLength = nums.length;

		while (index > 0) {
			if (nums[index] == nums[index - 1]) {
				newLength--;
				shiftLeft(nums, index, newLength);
				if (newLength <= 1) {
					return newLength;
				}
			}
			index--;
		}

		return newLength;
	}

	private void shiftLeft(int[] nums, int start, int length) {
		for (int index = start; index < length; index++) {
			nums[index] = nums[index + 1];
		}
	}

}
