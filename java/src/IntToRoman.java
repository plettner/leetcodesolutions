

public class IntToRoman {

	public String convert(int n) {
		return convertTens(n / 10) + convertOnes(n % 10);
	}

	String convertOnes(int input) {
		final String[] array = { "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" };
		final int n = input % 10;
		if (n >= 0 && n < 10) {
			return array[n];
		}
		throw new Error("Invalid value for convertOnes: " + input);
	}

	String convertTens(int input) {
		final String s = convertHundreds(input / 10);
		final String[] array = { "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" };
		final int n = input % 10;
		if (n >= 0 && n < 10) {
			return s + array[n];
		}
		throw new Error("Invalid value for convertTens: " + input);
	}

	String convertHundreds(int input) {
		final String s = convertThousands(input / 10);
		final String[] array = { "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" };
		final int n = input % 10;
		if (n >= 0 && n < 10) {
			return s + array[n];
		}
		throw new Error("Invalid value for convertHundreds: " + input);
	}

	String convertThousands(int input) {
		final int n = input % 10;
		final String[] array = { "", "M", "MM", "MMM" };
		if (n >= 0 && n < 4) {
			return array[n];
		}
		throw new Error("convertThousands: " + n + " is out of range; only support less than 4000");
	}

}
