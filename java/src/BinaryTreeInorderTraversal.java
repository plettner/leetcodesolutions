import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import utils.TreeNode;

public class BinaryTreeInorderTraversal {

	public List<Integer> inorderTraversal(TreeNode root) {

		return inorderTraversalIterative(root);
//		return inorderTraversalRecursive(root);

	}

	public List<Integer> inorderTraversalIterative(TreeNode root) {

		final Stack<TreeNode> stack = new Stack<>();
		final List<Integer> list = new LinkedList<>();

		while (root != null || !stack.isEmpty()) {

			while (root != null) {
				stack.push(root);
				root = root.left;
			}

			root = stack.pop();
			list.add(root.val);
			root = root.right;
		}

		return list;
	}

	public List<Integer> inorderTraversalRecursive(TreeNode root) {

		final List<Integer> list = new LinkedList<>();
		if (root == null) {
			return list;
		}

		final List<Integer> left = inorderTraversalRecursive(root.left);
		final List<Integer> right = inorderTraversalRecursive(root.right);

		if (left != null) {
			list.addAll(left);
		}

		list.add(root.val);

		if (right != null) {
			list.addAll(right);
		}

		return list;

	}

}
