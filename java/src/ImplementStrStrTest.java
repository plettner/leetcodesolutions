import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ImplementStrStrTest {

	private final ImplementStrStr strstr = new ImplementStrStr();

	@Test
	void example1() {
		final String haystack = "hello";
		final String needle = "ll";
		final int expected = 2;

		final int actual = this.strstr.strstr(haystack, needle);

		Assert.assertEquals(expected, actual);
	}

	@Test
	void example2() {
		final String haystack = "aaaaa";
		final String needle = "bba";
		final int expected = -1;

		final int actual = this.strstr.strstr(haystack, needle);

		Assert.assertEquals(expected, actual);
	}

	@Test
	void example3() {
		final String haystack = "";
		final String needle = "";
		final int expected = 0;

		final int actual = this.strstr.strstr(haystack, needle);

		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("Match Last Character")
	void matchLastCharacter() {
		final String haystack = "abcd";
		final String needle = "d";
		final int expected = 3;

		final int actual = this.strstr.strstr(haystack, needle);

		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("Longer Last Chars Match")
	void longLastCharsMatch() {
		final String haystack = "abcdefgh";
		final String needle = "defgh";
		final int expected = 3;

		final int actual = this.strstr.strstr(haystack, needle);

		Assert.assertEquals(expected, actual);

	}

	@Test
	void tailMatchTypical() {
		final String s = "abcdefgh";
		final String needle = "de";
		final int index = 4;
		final boolean expected = true;

		final boolean actual = this.strstr.tailMatch(s, index, needle);

		Assert.assertEquals(expected, actual);;
	}

	@Test
	void tailMatchEntireString() {
		final String s = "abcdefgh";
		final String needle = "abcdefgh";
		final int index = 1; // assume the first character matched
		final boolean expected = true;

		final boolean actual = this.strstr.tailMatch(s, index, needle);

		Assert.assertEquals(expected, actual);;
	}

	@Test
	void tailMatchFail() {
		final String s = "abcdefgh";
		final String needle = "de";
		final int index = 2;
		final boolean expected = false;

		final boolean actual = this.strstr.tailMatch(s, index, needle);

		Assert.assertEquals(expected, actual);;
	}

}
