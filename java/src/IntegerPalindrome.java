
public class IntegerPalindrome {

	int getFirstDigit(int input) {
		int n = Math.abs(input);
		if (n < 0) {
			throw new Error("Number exceeds problem statement limits");
		}

		while (n > 10) {
			n = n / 10;
		}

		return n;
	}

	int getLastDigit(int input) {
		final int n = Math.abs(input);
		if (n < 0) {
			throw new Error("Number exceeds problem statement limits");
		}

		return n % 10;
	}

	int countDigits(int input) {
		int count = 1;
		int n = Math.abs(input);
		if (n < 0) {
			throw new Error("Number exceeds problem statement limits");
		}

		while (n >= 10) {
			n = n / 10;
			count++;
		}

		return count;
	}

	// StackOverflow says this is the fastest way of doing powers of ten for ints
	static final int[] POWERS_OF_10 = { 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000 };

	static int powerOfTen(int pow) {
		if (pow >= 0) {
			return POWERS_OF_10[pow];
		}
		throw new Error("powerOfTen doesn't handle negative powers right now");
	}

	public boolean isPalindrome(int n) {

		// We're counting the negative sign as part of the palindrome,
		// and we can't have a trailing negative sign, so it always fails.
		// This means we could drop all the sign checks in sub-methods
		// but at this point, might as well leave 'em in there.
		if (n < 0) {
			return false;
		}

		// First get the number of digits in n
		int numDigits = countDigits(n);

		while (numDigits > 1) {
			// Second, get the first and last digits
			final int firstDigit = getFirstDigit(n);
			final int lastDigit = getLastDigit(n);

			if (firstDigit != lastDigit) {
				return false;
			}

			final int pow = powerOfTen(numDigits - 1);
			n = n - firstDigit * pow;
			n = n / 10;
			numDigits -= 2;
		}

		return true;
	}

	public boolean isPalindromeBetter(int input) {
		int r = 0;
		int n = input;

		if (n < 0) {
			return false; // negative numbers always fail by requirement
		}

		while (n > 0) {
			final int m = n % 10;
			r = (r * 10) + m;
			n = n / 10;
		}

		return input == r;
	}
}
