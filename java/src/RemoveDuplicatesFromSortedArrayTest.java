import org.junit.Assert;
import org.junit.jupiter.api.Test;

import utils.ArrayUtils;

class RemoveDuplicatesFromSortedArrayTest {

	private final RemoveDuplicatesFromSortedArray underTest = new RemoveDuplicatesFromSortedArray();

	@Test
	void example1() {
		final int[] actual = { 1, 1, 2 };
		final int[] expected = { 1, 2 };

		final int actualLength = this.underTest.removeDuplicates(actual);

		Assert.assertEquals(expected.length, actualLength);
		Assert.assertTrue(ArrayUtils.equalsFirstN(expected, actual, actualLength));

		// System.out.println(this.arrayUtils.toString(actual));
	}

	@Test
	void example2() {
		final int[] actual = { 0, 0, 1, 1, 1, 2, 2, 3, 3, 4 };
		final int[] expected = { 0, 1, 2, 3, 4 };

		final int actualLength = this.underTest.removeDuplicates(actual);

		Assert.assertEquals(expected.length, actualLength);
		Assert.assertTrue(ArrayUtils.equalsFirstN(expected, actual, actualLength));

		// System.out.println(this.arrayUtils.toString(actual));
	}

}
