import org.junit.Assert;
import org.junit.jupiter.api.Test;

import utils.ArrayUtils;

class MergeSortedArrayTest {

	private final MergeSortedArray msa = new MergeSortedArray();

	@Test
	void example1() {
		final int[] nums1 = { 1, 2, 3, 0, 0, 0 };
		final int num1Start = 3;
		final int[] nums2 = { 2, 5, 6 };
		final int num2Start = 3;
		final int[] expected = { 1, 2, 2, 3, 5, 6 };

		this.msa.merge(nums1, num1Start, nums2, num2Start);

		// System.out.println(ArrayUtils.toString(nums1));

		Assert.assertTrue(ArrayUtils.equals(expected, nums1));
	}

	@Test
	void example2() {
		final int[] nums1 = { 1 };
		final int num1Start = 1;
		final int[] nums2 = {};
		final int num2Start = 0;
		final int[] expected = { 1 };

		this.msa.merge(nums1, num1Start, nums2, num2Start);

		Assert.assertTrue(ArrayUtils.equals(expected, nums1));
	}

}
