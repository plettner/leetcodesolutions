import utils.ArrayUtils;

public class MergeSortedArray {

	/**
	 * Merge nums2 into nums1. nums1 is large enough to hold both nums1 and nums2 values.
	 *
	 * @param nums1 sorted array of numbers with empty spots at the end
	 * @param m     number of "populated" entries in the array
	 * @param nums2 sorted array of numbers
	 * @param n     number of entries
	 */
	public void merge(int[] nums1, int m, int[] nums2, int n) {
		int n2Index = 0;
		for (int index = 0; index < m && n2Index < n; index++) {
			if (nums2[n2Index] < nums1[index]) {
				ArrayUtils.shiftRight(nums1, index, m++);
				nums1[index] = nums2[n2Index];
				n2Index++;
			}
		}

		// If we have values left over in nums2, copy those over now
		while (n2Index < n) {
			nums1[m++] = nums2[n2Index++];
		}
	}

}
