import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TwoSumsTest {

	private final TwoSums twoSums = new TwoSums();

	private static final int ARRAY_SIZE = 10000;
	private final int[] expected = new int[2];
	private final int[] array = new int[ARRAY_SIZE];
	private int target = 0;

	@Test
	@DisplayName("Worst Case: Slower Solution")
	void worstCaseSlowerSolution() {
		this.expected[0] = ARRAY_SIZE - 2;
		this.expected[1] = ARRAY_SIZE - 1;
		for (int index = 0; index < this.array.length; index++) {
			this.array[index] = 100;
		}
		this.array[this.expected[0]] = 1;
		this.array[this.expected[1]] = 1;
		this.target = this.array[this.expected[0]] + this.array[this.expected[1]];

		final int[] actual = this.twoSums.twoSum(this.array, this.target);

		Assert.assertArrayEquals(this.expected, actual);
	}

	@Test
	@DisplayName("Best Case: Slower Solution")
	void bestCaseSlowerSolution() {
		this.expected[0] = 0;
		this.expected[1] = 1;
		for (int index = 0; index < this.array.length; index++) {
			this.array[index] = 100;
		}
		this.array[this.expected[0]] = 1;
		this.array[this.expected[1]] = 1;
		this.target = this.array[this.expected[0]] + this.array[this.expected[1]];

		final int[] actual = this.twoSums.twoSum(this.array, this.target);

		Assert.assertArrayEquals(this.expected, actual);
	}

	@Test
	@DisplayName("Medium Case: Slower Solution")
	void valuesMatchSlowerSolution() {
		this.expected[0] = ARRAY_SIZE / 3;
		this.expected[1] = ARRAY_SIZE / 3 * 2;
		for (int index = 0; index < this.array.length; index++) {
			this.array[index] = 100;
		}
		this.array[this.expected[0]] = 1;
		this.array[this.expected[1]] = 1;
		this.target = this.array[this.expected[0]] + this.array[this.expected[1]];

		final int[] actual = this.twoSums.twoSum(this.array, this.target);
		Assert.assertArrayEquals(this.expected, actual);
	}

	@Test
	@DisplayName("Throws Error: Slower Solution")
	void throwsErrorSlowerSolution() {
		this.expected[0] = 0;
		this.expected[1] = 1;
		for (int index = 0; index < this.array.length; index++) {
			this.array[index] = 100;
		}
		this.target = 0;

		Assertions.assertThrows(Error.class, () -> {
			this.twoSums.twoSum(this.array, this.target);
		});
	}

	@Test
	@DisplayName("Worst Case: Faster Solution")
	void worstCaseFasterSolution() {
		this.expected[0] = ARRAY_SIZE - 2;
		this.expected[1] = ARRAY_SIZE - 1;
		for (int index = 0; index < this.array.length; index++) {
			this.array[index] = 100;
		}
		this.array[this.expected[0]] = 1;
		this.array[this.expected[1]] = 1;
		this.target = this.array[this.expected[0]] + this.array[this.expected[1]];

		final int[] actual = this.twoSums.twoSumFaster(this.array, this.target);

		Assert.assertArrayEquals(this.expected, actual);
	}

	@Test
	@DisplayName("Best Case: Faster Solution")
	void bestCaseFasterSolution() {
		this.expected[0] = 0;
		this.expected[1] = 1;
		for (int index = 0; index < this.array.length; index++) {
			this.array[index] = 100;
		}
		this.array[this.expected[0]] = 1;
		this.array[this.expected[1]] = 1;
		this.target = this.array[this.expected[0]] + this.array[this.expected[1]];

		final int[] actual = this.twoSums.twoSumFaster(this.array, this.target);

		Assert.assertArrayEquals(this.expected, actual);
	}

	@Test
	@DisplayName("Medium Case: Faster Solution")
	void valuesMatchFasterSolution() {
		this.expected[0] = ARRAY_SIZE / 3;
		this.expected[1] = ARRAY_SIZE / 3 * 2;
		for (int index = 0; index < this.array.length; index++) {
			this.array[index] = 100;
		}
		this.array[this.expected[0]] = 1;
		this.array[this.expected[1]] = 1;
		this.target = this.array[this.expected[0]] + this.array[this.expected[1]];

		final int[] actual = this.twoSums.twoSumFaster(this.array, this.target);
		Assert.assertArrayEquals(this.expected, actual);
	}

	@Test
	@DisplayName("Throws Error: Faster Solution")
	void throwsErrorFasterSolution() {
		this.expected[0] = 0;
		this.expected[1] = 1;
		for (int index = 0; index < this.array.length; index++) {
			this.array[index] = 100;
		}
		this.target = 0;

		Assertions.assertThrows(Error.class, () -> {
			this.twoSums.twoSumFaster(this.array, this.target);
		});
	}

}
