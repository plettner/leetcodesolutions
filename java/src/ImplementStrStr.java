
public class ImplementStrStr {

	public int strstr(String haystack, String needle) {

		// If the needle is an empty string, requirements say to return 0
		if (needle == null || needle.length() == 0) {
			return 0;
		}

		if (haystack == null || haystack.length() < needle.length()) {
			return -1;
		}

		final char ch = needle.charAt(0);
		final int max = haystack.length() - (needle.length() - 1);
		for (int index = 0; index < max; index++) {
			if (ch == haystack.charAt(index)) {
				if (tailMatch(haystack, index + 1, needle)) {
					return index;
				}
			}
		}

		return -1;
	}

	/**
	 * Assumes we already tested to make sure that needle is not longer than (haystack.length() - index)
	 *
	 * @param s      our haystack
	 * @param index  starting point in the haystack (0 = first character)
	 * @param needle string that we're looking for the first occurance of
	 * @return
	 */
	boolean tailMatch(String s, int index, String needle) {

		// we know that the first characters match, so skip those
		for (int nIndex = 1; nIndex < needle.length(); nIndex++, index++) {
			if (s.charAt(index) != needle.charAt(nIndex)) {
				return false;
			}
		}

		return true;
	}
}
