import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class LengthOfLastWordTest {

	private final LengthOfLastWord lastWord = new LengthOfLastWord();

	@Test
	void example1() {
		final String value = "Hello World";
		final int expected = 5;

		final int actual = this.lastWord.lengthOfLastWord(value);

		Assert.assertEquals(expected, actual);
	}

	@Test
	void example2() {
		final String value = "   fly me   to   the moon  ";
		final int expected = 4;

		final int actual = this.lastWord.lengthOfLastWord(value);

		Assert.assertEquals(expected, actual);
	}

	@Test
	void example3() {
		final String value = "luffy is still joyboy";
		final int expected = 6;

		final int actual = this.lastWord.lengthOfLastWord(value);

		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("Empty String")
	void emptyString() {
		final String value = "";
		final int expected = 0;

		final int actual = this.lastWord.lengthOfLastWord(value);

		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("Spaces")
	void spaces() {
		final String value = "            ";
		final int expected = 0;

		final int actual = this.lastWord.lengthOfLastWord(value);

		Assert.assertEquals(expected, actual);
	}

}
