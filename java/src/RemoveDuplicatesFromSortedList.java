import utils.ListNode;

public class RemoveDuplicatesFromSortedList {

	public ListNode deleteDuplicates(ListNode head) {

		ListNode last = head;

		if (head == null || head.getNext() == null) {
			return head;
		}

		ListNode node = head.getNext();
		while (node != null) {

			// we found a duplicate. Drop "node" from the list.
			if (node.getValue() == last.getValue()) {
				node = node.getNext();
				last.setNext(node);
			} else {
				last = node;
				node = node.getNext();
			}
		}

		return head;
	}

}
