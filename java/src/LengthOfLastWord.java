
/**
 * This problem has a name that does not match the problem description. The
 * problem description clearly indicates it wants the length of the longest
 * word, not the last word.
 */
public class LengthOfLastWord {

	public int lengthOfLastWord(String s) {
		// Do the Java string split thing -- slower
		int n = this.lengthOfLastWordSplit(s);

		int nn = this.lengthOfLastWordLinear(s);

		if (nn != n) {
			throw new Error("Solutions got different results");
		}

		return n;
	}

	/**
	 * This solution should work in O(n) time
	 * 
	 * @param s string of words separated by spaces
	 * @return the length of the longest word
	 */
	public int lengthOfLastWordLinear(String s) {

		int maxWordLength = 0;

		if (s != null) {

			int index = 0;
			while (index < s.length()) {

				while (index < s.length() && Character.isWhitespace(s.charAt(index))) {
					index++;
				}

				int wordLength = 0;
				while (index < s.length() && !Character.isWhitespace(s.charAt(index))) {
					wordLength++;
					index++;
				}

				if (wordLength > maxWordLength) {
					maxWordLength = wordLength;
				}
			}
		}

		return maxWordLength;
	}

	/**
	 * Cleaner code, but it will have to do at least two passes over the string.
	 * 
	 * @param s string of words separated by spaces
	 * @return the length of the longest word in the string
	 */
	public int lengthOfLastWordSplit(String s) {

		// Split the string into its component words
		final String[] words = s.split(" ");

		// Words can't be of negative length, so start at zero
		int max = 0;
		for (final String word : words) {
			final int length = word.length();
			if (length > max) {
				max = length;
			}
		}

		return max;
	}

}