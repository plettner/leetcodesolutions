
public class SearchInsertPosition {

	private static final int BEFORE = -1;
	private static final int AFTER = 1;
	private static final int MATCH = 0;

	private int comparisonCount = 0;

	/**
	 * Find either the index of the target value in the sorted nums array, or find the index of the
	 * location where the target would be inserted. <strong>The runtime complexity should be O(log n)
	 * (which screams "binary search.")
	 *
	 * @param nums   an array of integers, sorted in ascending order
	 * @param target an integer value to search for or insert
	 * @return the index of where the target value is in the array, or the index of the position where
	 *         the target would be inserted.
	 */
	public int searchInsert(int[] nums, int target) {

		resetComparisonCount();

		int start = 0;
		int end = nums.length - 1;

		int index = start + (end - start) / 2;

		int result;

		while (start <= end) {
			result = compare(target, nums[index]);

			switch (result) {

			// Found it!
			case MATCH:
				return index;

			// The target is in the preceding half
			case BEFORE:
				end = index - 1;
				break;

			// The target is in the later half
			case AFTER:
				start = index + 1;
				break;
			}

			index = start + (end - start) / 2;
		}

		return index;
	}

	int compare(int target, int value) {
		incComparisonCount();
		return Integer.signum(target - value);
	}

	private void resetComparisonCount() {
		this.comparisonCount = 0;
	}

	private void incComparisonCount() {
		this.comparisonCount++;
	}

	public int getComparisonCount() {
		return this.comparisonCount;
	}
}
