import org.junit.Assert;
import org.junit.jupiter.api.Test;

import utils.ListNode;

class ReverseLinkedListTest {

	private final ReverseLinkedList reverseLinkedList = new ReverseLinkedList();

	@Test
	void example1() {
		final int[] array = { 1, 2, 3, 4, 5 };
		final int[] expectedArray = { 5, 4, 3, 2, 1 };

		final ListNode list = ListNode.fromArray(array);
		final ListNode expected = ListNode.fromArray(expectedArray);

		final ListNode actual = this.reverseLinkedList.reverse(list);

		Assert.assertTrue("Lists didn't match", ListNode.equals(expected, actual));
	}

	@Test
	void example2() {
		final int[] array = { 1, 2 };
		final int[] expectedArray = { 2, 1 };

		final ListNode list = ListNode.fromArray(array);
		final ListNode expected = ListNode.fromArray(expectedArray);

		final ListNode actual = this.reverseLinkedList.reverse(list);

		Assert.assertTrue("Lists didn't match", ListNode.equals(expected, actual));
	}

	@Test
	void example3() {
		final int[] array = {};
		final int[] expectedArray = {};

		final ListNode list = ListNode.fromArray(array);
		final ListNode expected = ListNode.fromArray(expectedArray);

		final ListNode actual = this.reverseLinkedList.reverse(list);

		Assert.assertTrue("Lists didn't match", ListNode.equals(expected, actual));
	}

}
