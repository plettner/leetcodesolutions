import org.junit.Assert;
import org.junit.jupiter.api.Test;

class MajorityElementTest {

	private final MajorityElement me = new MajorityElement();

	@Test
	void example1() {

		final int[] array = { 3, 2, 3 };
		final int expected = 3;

		final int actual = this.me.majorityElement(array);

		Assert.assertEquals(expected, actual);
	}

	@Test
	void example2() {

		final int[] array = { 2, 2, 1, 1, 1, 2, 2 };
		final int expected = 2;

		final int actual = this.me.majorityElement(array);

		Assert.assertEquals(expected, actual);
	}

}
