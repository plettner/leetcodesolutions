import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ReverseIntegerTest {

	private final ReverseInteger reverseInteger = new ReverseInteger();

	@Test
	@DisplayName("Example #1")
	void example1() {
		final int n = 123;
		final int expected = 321;
		final int actual = this.reverseInteger.reverseInteger(n);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("Example #2")
	void example2() {
		final int n = -123;
		final int expected = -321;
		final int actual = this.reverseInteger.reverseInteger(n);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("Example #3")
	void example3() {
		final int n = 120;
		final int expected = 21;
		final int actual = this.reverseInteger.reverseInteger(n);
		Assert.assertEquals(expected, actual);
	}

}
