import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This program/problem was described in a blog post that describes a problem someone was given when
 * interviewing with Shopify.
 *
 * <p>
 * <code>https://jigarius.com/blog/shopify-software-developer-interview</code>
 * </p>
 * <p>
 * In short, <code>asdw</code> are the control keys of a spaceship. a and d move the ship left and
 * right respectively (decrementing or incrementing x by one for each press.)
 * <p>
 * w will accelerate, increasing the speed by one. If one tries to accelerate beyond a max speed of
 * 5, a "maximum speed" message is displayed.
 * <p>
 * s will decelerate by one. Minimum speed is 1, and if one tries to go slower than 1, then the
 * message "minimum speed" is displayed.
 * <p>
 * When the ship reaches (0,250), then display "on the moon". If one goes beyond a y value of 250,
 * then "lost contact" is displayed.
 * <p>
 * The ship moves with every valid keypress. For each valid move, display the coordinates of the
 * ship.
 *
 * <p>
 * <strong>For this to work on Ubuntu, you have to change the terminal to raw mode.</strong>
 * </p>
 *
 * <p>
 * <code>stty raw</code>
 * </p>
 */

public class SpaceShip {
	public static void main(String [] args) throws IOException {
		final ReadConsole readConsole = new ReadConsole();
		readConsole.go();
	}
}

class ReadConsole {

	enum KeyPress {
		EXIT('x'), LEFT('a'), RIGHT('d'), ACCEL('w'), DECEL('s'), UNKNOWN('\0');

		private final int ch;

		KeyPress(int ch) {
			this.ch = ch;
		}

		public int getCh() {
			return this.ch;
		}

		public static KeyPress getKeyPress(int ch) {
			for (final KeyPress control : values()) {
				if (control.getCh() == ch) {
					return control;
				}
			}

			return UNKNOWN;
		}
	}

	private void log(String msg) {
		System.out.println(msg);
	}

	public void go() throws IOException {

		final Log log = new Log();
		final Ship ship = new Ship();
		boolean validKeyPress = false;

		log.log(ship.getStatus());

		KeyPress keyPress = getKeyPress();
		while (keyPress != KeyPress.EXIT) {
			validKeyPress = true;
			switch (keyPress) {
			case LEFT:
				ship.doLeft();
				break;
			case RIGHT:
				ship.doRight();
				break;
			case ACCEL:
				ship.doAccelerate();
				break;
			case DECEL:
				ship.doDecelerate();
				break;
			default:
				validKeyPress = false;
				break;
			}

			if (validKeyPress) {
				log(ship.getStatus());
			}

			keyPress = getKeyPress();
		}
	}

	private KeyPress getKeyPress() throws IOException {
		final int ch = ConsoleReader.getKeyPress();
		final KeyPress keyPress = KeyPress.getKeyPress(ch);
		return keyPress;
	}

}


class Ship {

	private static final int MAX_SPEED = 5;
	private static final int MIN_SPEED = 1;

	private static final int MOON_Y = 250;
	private static final int MOON_X = 0;

	private int speed = 0;
	private int x = 0;
	private int y = 0;

	private int move = 0;

	private final Log log = new Log();

	public int getMove() {
		return this.move;
	}

	public void setMove(int move) {
		this.move = move;
	}

	public void moveInc() {
		setMove(getMove() + 1);
	}

	public int getSpeed() {
		return this.speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int speedInc() {
		if (getSpeed() >= MAX_SPEED) {
			this.log.log("maximum speed");
		} else {
			setSpeed(getSpeed() + 1);
		}
		return this.speed;
	}

	public int speedDec() {
		if (getSpeed() <= MIN_SPEED) {
			this.log.log("minimum speed");
		} else {
			setSpeed(getSpeed() - 1);
		}
		return this.speed;
	}

	public void doLeft() {
		this.x = this.x - 1;
		update();
	}

	public void doRight() {
		this.x = this.x + 1;
		update();
	}

	public void doAccelerate() {
		speedInc();
		update();
	}

	public void doDecelerate() {
		speedDec();
		update();
	}

	private void update() {
		moveInc();
		this.y = this.y + this.speed;
	}

	public String getStatus() {
		if (this.x == 0 && this.y == 0 && getMove() == 0) {
			return "(0,0) ready for launch";
		} else if (this.y > MOON_Y) {
			return "contact lost";
		} else if (this.y == MOON_Y && this.x == MOON_X) {
			return "on the moon";
		} else {
			final String s = String.format("(%d,%d)", this.x, this.y);
			return s;
		}
	}

}

class ConsoleReader {
	private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in), 1);

	static int getKeyPress() throws IOException {
		return reader.read();
	}
}

class Log {
	public void log(String msg) {
		System.out.println("\n" + msg);
	}
}
