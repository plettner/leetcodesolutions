import java.util.HashMap;
import java.util.Map;

/**
 * Possible non O(n-squared) solution:
 * <ol>
 * <li>Create map of values mapped to their indeces</li>
 * <li>Step through the array; for each n[index]
 * <ol>
 * <li>search for the solution answer in the map</li>
 * <li>return the indeces of each value Uses a lot of space though</li>
 * </ol>
 * </ol>
 */
class TwoSums {

	private final int[] answer = new int[2];

	private int[] createAnswer(int v1, int v2) {
		this.answer[0] = v1;
		this.answer[1] = v2;
		return this.answer;

	}

	public int[] twoSum(int[] nums, int target) {

		for (int outer = 0; outer < nums.length; outer++) {
			for (int inner = 0; inner < nums.length; inner++) {
				if (nums[outer] + nums[inner] == target) {
					if (outer != inner) {
						return createAnswer(outer, inner);
					}
				}
			}
		}

		throw new Error("Could not find a solution; you must have a bug.");
	}

	public int[] twoSumFaster(int[] nums, int target) {

		final Map<Integer, Integer> map = new HashMap<Integer, Integer>();

		int outer = 0;
		for (int inner = outer + 1; inner < nums.length; inner++) {
			map.put(nums[inner], inner);
			if (outer != inner) {
				if (nums[outer] + nums[inner] == target) {
					return createAnswer(outer, inner);
				}
			}
		}

		for (outer = 1; outer < nums.length; outer++) {
			final int subTarget = target - nums[outer];
			final Integer inner = map.get(subTarget);
			if (inner != null) {
				return createAnswer(outer, inner);
			}
		}

		throw new Error("No solution could be found; you must have a bug.");
	}

}