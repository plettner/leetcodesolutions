
public class MaximumSubarray {

	public int maxSubArrayDivideAndConquer(int[] nums) {
		final int start = 0;
		final int end = nums.length - 1;
		final int maxSum = maxSumRecurse(nums, start, end);
		return maxSum;
	}

	/**
	 * My attempt at a divide and conquer solution.
	 *
	 * @param nums array of integers
	 * @return maximum sum in any contiguous sub array
	 */
	public int maxSumRecurse(int[] nums, int start, int end) {

		if (start >= end) {
			return nums[start];
		}

		int maxSum = Integer.MIN_VALUE;

		final int mid = (start + end) / 2;

		final int sumLeft = maxSumRecurse(nums, start, mid);
		final int sumRight = maxSumRecurse(nums, mid + 1, end);

		final int sum = sumLeft + sumRight;
		if (sum > maxSum) {
			maxSum = sum;
		}
		if (sumLeft > maxSum) {
			maxSum = sumLeft;
		}
		if (sumRight > maxSum) {
			maxSum = sumRight;
		}
		return maxSum;
	}

	/**
	 * Returns a contiguous subarray with at least one number which has the largest sum. The more
	 * obvious solution has a time complexity of O(n) where a more subtle solution uses the divide and
	 * conquer approach.
	 *
	 * <strong>Note:</strong> I haven't figured out the "divide and conquer" solution.
	 *
	 * @param nums an array of integers where each number is between -10^5 and 10^5
	 * @return the maximum sum of that subarray
	 */
	public int maxSubArray(int[] nums) {
		int maxSum = Integer.MIN_VALUE;

		// System.out.println("======================");

		for (int start = 0; start < nums.length; start++) {
			for (int end = start; end < nums.length; end++) {
				final int sum = computeSum(nums, start, end);
				if (sum > maxSum) {
					maxSum = sum;
				}
			}
		}

		return maxSum;
	}

	/**
	 * Computes the sum of all integers in the subarray from start to end
	 *
	 * @param nums  an array of ints
	 * @param start starting index from which to start summing
	 * @param end   index of last spot
	 * @return the sum of this subarray
	 */
	int computeSum(int[] nums, final int start, final int end) {
		int sum = nums[start];
		for (int index = start + 1; index <= end; index++) {
			sum += nums[index];
		}

		log(sum, start, end);

		return sum;
	}

	void log(int sum, int start, int end) {
		// System.out.println(String.format("Sum=%d, start=%d, end=%d", sum, start, end));
	}
}
