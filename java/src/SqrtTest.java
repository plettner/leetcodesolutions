import org.junit.Assert;
import org.junit.jupiter.api.Test;

class SqrtTest {

	private final Sqrt sqrt = new Sqrt();

	@Test
	void example1() {
		final int value = 4;
		final int expected = 2;

		final int actual = this.sqrt.mySqrt(value);

		Assert.assertEquals(expected, actual);
	}

	@Test
	void example2() {
		final int value = 8;
		final int expected = 2;

		final int actual = this.sqrt.mySqrt(value);

		Assert.assertEquals(expected, actual);
	}

}
