
/**
 * You are climbing a staircase. It takes n steps to reach the top.
 *
 * Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top.
 */
public class ClimbingStairs {

	/**
	 * Turns out it's a representation of the Fibonacci sequence.
	 *
	 * This routine can be improved by using a map to store preliminary results.
	 *
	 * @param n number of steps on the stairs.
	 *
	 * @return the number of combinations of one and two steps to get to the top
	 */
	public int climbStairs(int n) {
		return climbStairsIter(n);
	}

	public int climbStairsRecurse(int n) {

		if (n == 1) {
			return 1;
		}

		if (n == 2) {
			return 2;
		}

		return climbStairs(n - 1) + climbStairs(n - 2);

	}

	public int climbStairsIter(int n) {

		int previous = 1;
		int current = 1;
		int tmp = 0;

		for (int index = 1; index < n; index++) {
			tmp = current;
			current = current + previous;
			previous = tmp;
		}

		return current;

	}
}
