import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class IntegerPalindromeTest {

	private final IntegerPalindrome integerPalindrome = new IntegerPalindrome();

	@Test
	@DisplayName("isPalindrome: Example #1")
	void isPalindromeExampleNumber1() {
		final int value = 121;
		final boolean expected = true;
		final boolean actual = this.integerPalindrome.isPalindrome(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("isPalindrome: Example #2")
	void isPalindromeExampleNumber2() {
		final int value = -121;
		final boolean expected = false;
		final boolean actual = this.integerPalindrome.isPalindrome(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("isPalindrome: Example #3")
	void isPalindromeExampleNumber3() {
		final int value = 10;
		final boolean expected = false;
		final boolean actual = this.integerPalindrome.isPalindrome(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("isPalindrome: Example #4")
	void isPalindromeExampleNumber4() {
		final int value = -101;
		final boolean expected = false;
		final boolean actual = this.integerPalindrome.isPalindrome(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("isPalindrome: 1234321")
	void isPalindrome1234321() {
		final int value = 1234321;
		final boolean expected = true;
		final boolean actual = this.integerPalindrome.isPalindrome(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("isPalindrome: 1234554321")
	void isPalindrome1234554321() {
		final int value = 1234321;
		final boolean expected = true;
		final boolean actual = this.integerPalindrome.isPalindrome(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("isPalindrome: 11")
	void isPalindrome11() {
		final int value = 11;
		final boolean expected = true;
		final boolean actual = this.integerPalindrome.isPalindrome(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("getFirstDigit: max")
	void getFirstDigitMax() {
		final int expected = 2;
		final int actual = this.integerPalindrome.getFirstDigit(Integer.MAX_VALUE);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("getFirstDigit: min")
	void getFirstDigitMin() {
		final int expected = 2;
		final int actual = this.integerPalindrome.getFirstDigit(Integer.MIN_VALUE + 1);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("getFirstDigit: zero")
	void getFirstDigitZero() {
		final int expected = 0;
		final int actual = this.integerPalindrome.getFirstDigit(0);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("countDigits: max")
	void countDigitsMax() {
		final int expected = 10;
		final int actual = this.integerPalindrome.countDigits(Integer.MAX_VALUE);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("countDigits: zero")
	void countDigitsZero() {
		final int expected = 1;
		final int actual = this.integerPalindrome.countDigits(0);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("countDigits: max")
	void countDigitsMin() {
		final int expected = 10;
		final int actual = this.integerPalindrome.countDigits(Integer.MIN_VALUE + 1);
		Assert.assertEquals(expected, actual);
	}

	/**
	 * Bit of an edge case because of > vs >=
	 */
	@Test
	@DisplayName("countDigits: 10")
	void countDigits10() {
		final int expected = 2;
		final int actual = this.integerPalindrome.countDigits(10);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("isPalindromeBetter: Example #1")
	void isPalindromeBetterExampleNumber1() {
		final int value = 121;
		final boolean expected = true;
		final boolean actual = this.integerPalindrome.isPalindromeBetter(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("isPalindromeBetter: Example #2")
	void isPalindromeBetterExampleNumber2() {
		final int value = -121;
		final boolean expected = false;
		final boolean actual = this.integerPalindrome.isPalindromeBetter(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("isPalindromeBetter: Example #3")
	void isPalindromeBetterExampleNumber3() {
		final int value = 10;
		final boolean expected = false;
		final boolean actual = this.integerPalindrome.isPalindromeBetter(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("isPalindromeBetter: Example #4")
	void isPalindromeBetterExampleNumber4() {
		final int value = -101;
		final boolean expected = false;
		final boolean actual = this.integerPalindrome.isPalindromeBetter(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("isPalindromeBetter: 1234321")
	void isPalindromeBetter1234321() {
		final int value = 1234321;
		final boolean expected = true;
		final boolean actual = this.integerPalindrome.isPalindromeBetter(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("isPalindromeBetter: 1234554321")
	void isPalindromeBetter1234554321() {
		final int value = 1234321;
		final boolean expected = true;
		final boolean actual = this.integerPalindrome.isPalindromeBetter(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("isPalindromeBetter: 11")
	void isPalindromeBetter11() {
		final int value = 11;
		final boolean expected = true;
		final boolean actual = this.integerPalindrome.isPalindromeBetter(value);
		Assert.assertEquals(expected, actual);
	}

}
