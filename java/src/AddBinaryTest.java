import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class AddBinaryTest {

	private final AddBinary addBinary = new AddBinary();

	@Test
	void example1() {
		final String a = "11";
		final String b = "1";
		final String expected = "100";

		final String actual = this.addBinary.addBinary(a, b);

		Assert.assertEquals(expected, actual);
	}

	@Test
	void example2() {
		final String a = "1010";
		final String b = "1011";
		final String expected = "10101";

		final String actual = this.addBinary.addBinary(a, b);

		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("7 + 7")
	void sevenPlusSeven() {
		final String a = "111";
		final String b = "111";
		final String expected = "1110";

		final String actual = this.addBinary.addBinary(a, b);

		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("0 + 0")
	void zeroPlusZero() {
		final String a = "0";
		final String b = "0";
		final String expected = "0";

		final String actual = this.addBinary.addBinary(a, b);

		Assert.assertEquals(expected, actual);
	}

}
