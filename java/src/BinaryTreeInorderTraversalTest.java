import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import utils.TreeNode;

class BinaryTreeInorderTraversalTest {

	private final BinaryTreeInorderTraversal cut = new BinaryTreeInorderTraversal();

	@Test
	void example1() {
		final Integer[] nums = { 1, null, 2, 3 };
		final int[] expected = { 1, 3, 2 };

		final TreeNode tree = TreeNode.createFromArray(nums);

		final List<Integer> actual = this.cut.inorderTraversal(tree);

		Assert.assertNotNull(actual);

		int index = 0;
		for (final Integer i : actual) {
			Assert.assertEquals(expected[index++], (int) i);
		}
	}

	@Test
	void example2() {
		final Integer[] nums = {};
		final int[] expected = {};

		final TreeNode tree = TreeNode.createFromArray(nums);

		final List<Integer> actual = this.cut.inorderTraversal(tree);

		Assert.assertNotNull(actual);

		int index = 0;
		for (final Integer i : actual) {
			Assert.assertEquals(expected[index++], (int) i);
		}
	}

	@Test
	void example3() {
		final Integer[] nums = { 1 };
		final int[] expected = { 1 };

		final TreeNode tree = TreeNode.createFromArray(nums);

		final List<Integer> actual = this.cut.inorderTraversal(tree);

		Assert.assertNotNull(actual);

		int index = 0;
		for (final Integer i : actual) {
			Assert.assertEquals(expected[index++], (int) i);
		}
	}

	@Test
	void example4() {
		final Integer[] nums = { 1, 2 };
		final int[] expected = { 2, 1 };

		final TreeNode tree = TreeNode.createFromArray(nums);

		final List<Integer> actual = this.cut.inorderTraversal(tree);

		Assert.assertNotNull(actual);

		int index = 0;
		for (final Integer i : actual) {
			Assert.assertEquals(expected[index++], (int) i);
		}
	}

	@Test
	void example5() {
		final Integer[] nums = { 1, null, 2 };
		final int[] expected = { 1, 2 };

		final TreeNode tree = TreeNode.createFromArray(nums);

		final List<Integer> actual = this.cut.inorderTraversal(tree);

		Assert.assertNotNull(actual);

		int index = 0;
		for (final Integer i : actual) {
			Assert.assertEquals(expected[index++], (int) i);
		}
	}
}
