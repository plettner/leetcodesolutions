import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class IntToRomanTest {

	private final IntToRoman intToRoman = new IntToRoman();
	private final RomanToInteger romanToInt = new RomanToInteger();

	@Test
	@DisplayName("0001")
	void test0001() {
		final int value = 1;
		final String expected = "I";
		final String actual = this.intToRoman.convert(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("2051")
	void test2051() {
		final int value = 2051;
		final String expected = "MMLI";
		final String actual = this.intToRoman.convert(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("2888")
	void test2888() {
		final int value = 2888;
		final String expected = "MMDCCCLXXXVIII";
		final String actual = this.intToRoman.convert(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("3444")
	void test3444() {
		final int value = 3444;
		final String expected = "MMMCDXLIV";
		final String actual = this.intToRoman.convert(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("1777")
	void test1777() {
		final int value = 1777;
		final String expected = "MDCCLXXVII";
		final String actual = this.intToRoman.convert(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("1666")
	void test1666() {
		final int value = 1666;
		final String expected = "MDCLXVI";
		final String actual = this.intToRoman.convert(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("Int to Roman to Int")
	void intToRomanToInt() {
		for (int index = 1; index < 4000; index++) {
			final String roman = this.intToRoman.convert(index);
			final int result = this.romanToInt.convert(roman);
			Assert.assertEquals(index, result);
		}
	}
}
