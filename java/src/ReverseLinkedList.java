import utils.ListNode;

public class ReverseLinkedList {

	ListNode reverse(ListNode head) {

		if (head == null || head.next == null) {
			return head; // already reversed
		}

		final ListNode tail = reverse(head.next);
		head.next.next = head;

		head.next = null;

		return tail;
	}

}
