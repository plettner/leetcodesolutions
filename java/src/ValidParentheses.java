import java.util.Stack;

public class ValidParentheses {

	private final Stack<Character> stack = new Stack<>();

	public boolean isValid(String s) {

		for (final char ch : s.toCharArray()) {
			if (isOpenParen(ch)) {
				this.stack.push(ch);
				continue;
			}
			if (isCloseParen(ch)) {
				final char chOpen = this.stack.peek();
				if (match(chOpen, ch)) {
					this.stack.pop();
					continue;
				}

				// Closing paren didn't match type of open paren; fail
				return false;
			}
		}

		// An empty stack means that we matched all parens
		return this.stack.size() == 0;
	}

	private boolean isOpenParen(char ch) {
		return (ch == '(' || ch == '[' || ch == '{');
	}

	private boolean isCloseParen(char ch) {
		return ch == ')' || ch == ']' || ch == '}';
	}

	private boolean match(char open, char close) {
		if (open == '(' && close == ')') {
			return true;
		}

		if (open == '[' && close == ']') {
			return true;
		}

		if (open == '{' && close == '}') {
			return true;
		}

		return false;
	}

}
