
public class ContainsDuplicateII {

	public boolean containsNearbyDuplicate(int[] nums, int k) {

		for (int outer = 0; outer < nums.length; outer++) {

			int max = outer + k;
			if (max > nums.length - 1) {
				max = nums.length - 1;
			}

			for (int inner = max; inner > outer; inner--) {
				if (nums[outer] == nums[inner]) {
					return true;
				}
			}
		}

		return false;
	}

}
