
public class MajorityElement {

	public int majorityElement(int[] nums) {

		final int n = majorityElementNSquared(nums);

		final int r = majorityElementRecursive(nums);

		if (n != r) {
			throw new Error("Solutions do not produce the same answer");
		}

		return n;
	}

	public int majorityElementRecursive(int[] nums) {
		if (nums.length == 0) {
			throw new Error("Invalid input");
		}

		return majorityElementRecursiveSub(nums, 0, nums.length - 1);
	}

	public Integer majorityElementRecursiveSub(int[] nums, int start, int end) {

		if (start == end) {
			return nums[start];
		}

		final int h = (start + end - 1) / 2;

		final Integer l = majorityElementRecursiveSub(nums, start, h);
		final Integer r = majorityElementRecursiveSub(nums, h + 1, end);

		if (l == null && r == null) {
			return null;
		} else if (l == null) {
			return r;
		} else if (r == null) {
			return l;
		} else if (l != r) {
			return null;
		}

		return l;
	}

	public int majorityElementNSquared(int[] nums) {

		int max = 0;
		int majorityElement = 0;
		final int nOver2 = (nums.length / 2) + (nums.length % 2);

		for (int outer = 0; outer < nums.length; outer++) {

			final int currentNum = nums[outer];

			int count = 0;
			for (int inner = 0; inner < nums.length; inner++) {
				if (currentNum == nums[inner]) {
					count++;
				}
			}

			if (count > max) {
				max = count;
				majorityElement = currentNum;

				if (max >= nOver2) {
					return majorityElement;
				}
			}
		}

		throw new Error("This problem solution should always yeild a majority element");

	}
}
