import org.junit.Assert;
import org.junit.jupiter.api.Test;

class ContainsDuplicateIITest {

	private final ContainsDuplicateII cd = new ContainsDuplicateII();

	@Test
	void example1() {
		final int[] nums = { 1, 2, 3, 1 };
		final int k = 3;
		final boolean expected = true;

		final boolean actual = this.cd.containsNearbyDuplicate(nums, k);

		Assert.assertEquals(expected, actual);
	}

	@Test
	void example2() {
		final int[] nums = { 1, 0, 1, 1 };
		final int k = 1;
		final boolean expected = true;

		final boolean actual = this.cd.containsNearbyDuplicate(nums, k);

		Assert.assertEquals(expected, actual);
	}

	@Test
	void example3() {
		final int[] nums = { 1, 2, 3, 1, 2, 3 };
		final int k = 2;
		final boolean expected = false;

		final boolean actual = this.cd.containsNearbyDuplicate(nums, k);

		Assert.assertEquals(expected, actual);
	}

}
