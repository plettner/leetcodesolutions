import java.util.Stack;

import utils.TreeNode;

public class SameTree {

	public boolean isSameTree(TreeNode p, TreeNode q) {

		final Stack<TreeNode> stack1 = new Stack<>();
		final Stack<TreeNode> stack2 = new Stack<>();

		TreeNode root1 = p;
		TreeNode root2 = q;

		while ((root1 != null || root2 != null) || (!stack1.isEmpty() && !stack2.isEmpty())) {

			while (root1 != null && root2 != null) {
				if (root1.val != root2.val) {
					return false;
				}
				stack1.push(root1);
				root1 = root1.left;
				stack2.push(root2);
				root2 = root2.left;
			}

			// Both should have found nulls on the left; if not, unequal
			if (root1 != null || root2 != null) {
				return false;
			}

			root1 = stack1.pop();
			root2 = stack2.pop();
			root1 = root1.right;
			root2 = root2.right;

		}

		if (!stack1.empty() || !stack2.empty()) {
			return false;
		}

		return true;
	}

}
