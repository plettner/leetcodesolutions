import org.junit.Assert;
import org.junit.jupiter.api.Test;

class PlusOneTest {

	private final PlusOne plusOne = new PlusOne();

	@Test
	void example1() {
		final int[] digits = { 1, 2, 3 };
		final int[] expected = { 1, 2, 4 };

		final int[] actual = this.plusOne.plusOne(digits);

		Assert.assertArrayEquals(expected, actual);
	}

	@Test
	void example2() {
		final int[] digits = { 4, 3, 2, 1 };
		final int[] expected = { 4, 3, 2, 2 };

		final int[] actual = this.plusOne.plusOne(digits);

		Assert.assertArrayEquals(expected, actual);
	}

	@Test
	void example3() {
		final int[] digits = { 0 };
		final int[] expected = { 1 };

		final int[] actual = this.plusOne.plusOne(digits);

		Assert.assertArrayEquals(expected, actual);
	}

}
