
public class Sqrt {

	public int mySqrt(int n) {
		return mySqrtIter(n);
	}

	public int mySqrtRecursive(int x) {

		if (x < 0) {
			throw new Error("Invalid input");
		}

		if (x < 2) {
			return x;
		}

		final int small = mySqrt(x >> 2) << 1;
		final int large = small + 1;

		if (large * large > x) {
			return small;
		}

		return large;
	}

	public int mySqrtIter(int n) {

		if (n < 0) {
			throw new Error("Invalid input");
		}

		if (n < 2) {
			return n;
		}

		int shift = 2;
		while ((n >> shift) != 0) {
			shift += 2;
		}

		int result = 0;

		while (shift >= 0) {
			result = result << 1;
			final int large = (result + 1);
			if (large * large <= n >> shift) {
				result = large;
			}
			shift -= 2;
		}

		return result;
	}

}
