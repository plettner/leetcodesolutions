
public class ReverseInteger {

	public int reverseInteger(int n) {
		int result = 0;
		final boolean negative = n < 0;
		n = Math.abs(n);
		while (n > 0) {
			final int m = n % 10;
			result = (result * 10) + m;
			n = n - m;
			n = n / 10;
		}
		return negative ? -result : result;
	}
}
