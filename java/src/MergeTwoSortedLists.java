import utils.ListNode;

public class MergeTwoSortedLists {

	public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
		return ListNode.merge(l1, l2);
	}

}
