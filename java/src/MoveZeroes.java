
public class MoveZeroes {

	void moveZeroes(int[] nums) {
		for (int index = 0, j = 0; index < nums.length; index++) {
			if (nums[index] != 0) {
				final int tmp = nums[index];
				nums[index] = nums[j];
				nums[j++] = tmp;
			}
		}
	}

}
