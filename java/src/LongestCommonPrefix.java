
public class LongestCommonPrefix {

	private static final int MAX_STR_LENGTH = 200;

	public String process(String[] strs) {

		if (strs.length == 0) {
			return "";
		}
		if (strs.length == 1) {
			return strs[0];
		}

		final int minLength = getMinLength(strs);
		if (minLength == 0) {
			return "";
		}

		boolean found = true;
		for (int cIndex = 0; cIndex < minLength; cIndex++) {
			final char ch = strs[0].charAt(cIndex);
			for (int sIndex = 1; sIndex < strs.length; sIndex++) {
				final char ch2 = strs[sIndex].charAt(cIndex);
				if (ch != ch2) {
					found = false;
					break;
				}
			}

			if (!found) {
				if (cIndex == 0) {
					return "";
				}
				return strs[0].substring(0, cIndex);
			}
		}

		return strs[0].substring(0, minLength);
	}

	private int getMinLength(String[] strs) {
		int min = MAX_STR_LENGTH;
		for (int index = 0; index < strs.length; index++) {
			final int len = strs[index].length();
			if (len < min) {
				min = len;
			}
		}
		return min;
	}

}
