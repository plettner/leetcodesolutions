import org.junit.Assert;
import org.junit.jupiter.api.Test;

class FactorialTrailingZeroesTest {

	private final FactorialTrailingZeroes ftz = new FactorialTrailingZeroes();

	@Test
	void example1() {
		final int n = 3;
		final int expected = 0;

		final int actual = this.ftz.trailingZeroes(n);

		Assert.assertEquals(expected, actual);
	}

	@Test
	void example2() {
		final int n = 5;
		final int expected = 1;

		final int actual = this.ftz.trailingZeroes(n);

		Assert.assertEquals(expected, actual);
	}

	@Test
	void example3() {
		final int n = 0;
		final int expected = 0;

		final int actual = this.ftz.trailingZeroes(n);

		Assert.assertEquals(expected, actual);
	}

}
