import org.junit.Assert;
import org.junit.jupiter.api.Test;

import utils.ListNode;

class MergeTwoSortedListsTest {

	private final MergeTwoSortedLists merge = new MergeTwoSortedLists();

	@Test
	void example1() {
		final int[] array1 = { 1, 2, 4 };
		final int[] array2 = { 1, 3, 4 };
		final int[] expectedArray = { 1, 1, 2, 3, 4, 4 };

		final ListNode list1 = ListNode.fromArray(array1);
		final ListNode list2 = ListNode.fromArray(array2);
		final ListNode expected = ListNode.fromArray(expectedArray);

		final ListNode actual = this.merge.mergeTwoLists(list1, list2);

		Assert.assertTrue(expected.equals(actual));
	}

	@Test
	void example2() {
		final int[] array1 = {};
		final int[] array2 = {};
		final int[] expectedArray = {};

		final ListNode list1 = ListNode.fromArray(array1);
		final ListNode list2 = ListNode.fromArray(array2);
		final ListNode expected = ListNode.fromArray(expectedArray);

		Assert.assertNull(list1);
		Assert.assertNull(list2);
		Assert.assertNull(expected);

		final ListNode actual = this.merge.mergeTwoLists(list1, list2);

		Assert.assertTrue(ListNode.equals(expected, actual));
	}

	@Test
	void example3() {
		final int[] array1 = {};
		final int[] array2 = { 0 };
		final int[] expectedArray = { 0 };

		final ListNode list1 = ListNode.fromArray(array1);
		final ListNode list2 = ListNode.fromArray(array2);
		final ListNode expected = ListNode.fromArray(expectedArray);

		final ListNode actual = this.merge.mergeTwoLists(list1, list2);

		Assert.assertFalse(ListNode.equals(expected, actual));
	}

}
