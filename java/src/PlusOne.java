
public class PlusOne {

	public int[] plusOne(int[] digits) {

		if (digits == null || digits.length == 0) {
			throw new Error("Array cannot be empty or null");
		}

		final int last = digits.length - 1;

		digits[last]++;

		return digits;
	}

}
