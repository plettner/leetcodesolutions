
public class AddBinary {

	private static final char ONE = '1';
	private static final char ZERO = '0';

	public String addBinary(String a, String b) {

		if (a == null || a.trim().length() == 0 || b == null || b.trim().length() == 0) {
			throw new Error("Invalid input");
		}

		// Ensure that the second string is the shorter of the two
		if (b.length() > a.length()) {
			final String tmp = a;
			a = b;
			b = tmp;
		}

		final StringBuilder sb = new StringBuilder();

		int bindex = b.length() - 1;
		int carry = 0;
		for (int index = a.length() - 1; index >= 0; index--) {
			final int aval = a.charAt(index) == ONE ? 1 : 0;
			int bval = 0;
			if (bindex >= 0) {
				bval = b.charAt(bindex--) == ONE ? 1 : 0;
			}
			switch (bval + aval + carry) {
			case 3:
				carry = 1;
				sb.insert(0, ONE);
				break;
			case 2:
				carry = 1;
				sb.insert(0, ZERO);
				break;
			case 1:
				carry = 0;
				sb.insert(0, ONE);
				break;
			case 0:
				carry = 0;
				sb.insert(0, ZERO);
				break;
			default:
				throw new Error("Can't have a result greater than 3 for binary addition");
			}
		}

		if (carry == 1) {
			sb.insert(0, ONE);
		}

		return sb.toString();

	}

}
