package utils;

public class TreeNode {
	public int val;
	public TreeNode left;
	public TreeNode right;

	public TreeNode() {
	}

	public TreeNode(int val) {
		this.val = val;
	}

	public TreeNode(int val, TreeNode left, TreeNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}

	public static TreeNode createFromArray(Integer[] array) {

		if (array.length == 0) {
			return null;
		}

		final TreeNode NULL_NODE = new TreeNode(Integer.MAX_VALUE);
		final TreeNode head = new TreeNode(array[0]);
		TreeNode newNode = NULL_NODE;
		TreeNode node = head;
		for (int index = 1; index < array.length; index++) {
			final Integer n = array[index];
			newNode = n == null ? NULL_NODE : new TreeNode(n);

			if (node.left == null) {
				node.left = newNode;
			} else {
				if (n != null) {
					node.right = newNode;
				}

				if (node.left == NULL_NODE) {
					node.left = null;
					node = node.right;
				} else {
					node = node.left;
				}
			}
		}

		return head;
	}

	@Override
	public String toString() {
		return "" + this.val;
	}

}
