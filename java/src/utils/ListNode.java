package utils;

/**
 * Used by LeetCode to support a few of their problems that involve linked lists
 */
public class ListNode {

	public int val;
	public ListNode next;

	public ListNode() {
	}

	public ListNode(int val) {
		this.val = val;
	}

	public int getValue() {
		return this.val;
	}

	public void setValue(int value) {
		this.val = value;
	}

	public ListNode getNext() {
		return this.next;
	}

	public void setNext(ListNode listNode) {
		this.next = listNode;
	}

	public ListNode(int val, ListNode next) {
		this.val = val;
		this.next = next;
	}

	/**
	 * Create a ListNode list from an array of ints.
	 *
	 * @param array an array of integers
	 * @return the array as a list of ListNode objects
	 */
	public static ListNode fromArray(int[] array) {

		ListNode head = null;
		ListNode last = null;

		for (final int i : array) {
			final ListNode node = new ListNode(i);
			if (head == null) {
				head = node;
			}
			if (last != null) {
				last.setNext(node);
			}
			last = node;
		}

		return head;
	}

	public static ListNode append(ListNode list, ListNode tail) {

		if (list == null) {
			return tail;
		}

		list = list.getLastNode();
		if (list != null) {
			list.setNext(tail);
			return list;
		}

		return tail;
	}

	public ListNode getLastNode() {
		ListNode list = this;
		while (list != null && list.getNext() != null) {
			list = list.getNext();
		}
		return list;
	}

	public static ListNode merge(ListNode list1, ListNode list2) {

		ListNode list = null;
		ListNode listLast = null;
		ListNode next = null;

		while (list1 != null && list2 != null) {
			if (list1.getValue() < list2.getValue()) {
				next = list1.getNext();
				if (list == null) {
					list = list1;
					listLast = list1;
				} else {
					listLast.setNext(list1);
					listLast = list1;
				}
				list1 = next;
			} else {
				next = list2.getNext();
				if (list == null) {
					list = list2;
					listLast = list2;
				} else {
					listLast.setNext(list2);
					listLast = list2;
				}
				list2 = next;
			}
		}

		if (list1 != null) {
			append(list, list1);
		} else {
			if (list2 != null) {
				ListNode.append(list, list2);
			}
		}

		return list;
	}

	/**
	 * ListNode arrays (in this exercise) are equal if
	 * <ol>
	 * <li>arrays are the same length</li>
	 * <li>arrays are both in the same order</li>
	 * <li>corresponding elements have the same value
	 * <li>
	 * </ol>
	 *
	 * @param right the list to compare it to
	 * @return <code>true</code> if the lists are equal and <code>false</code> if not.
	 */
	public boolean equals(ListNode right) {
		ListNode left = this;

		while (left != null && right != null) {
			if (left.val != right.val) {
				return false;
			}
			left = left.next;
			right = right.next;
		}

		return left == null && right == null;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append('[');

		ListNode listNode = this;
		do {
			sb.append(listNode.getValue());

			final ListNode next = listNode.getNext();
			if (next != null) {
				sb.append(", ");
			}
			listNode = next;

		} while (listNode != null);

		sb.append(']');
		return sb.toString();
	}

	public static boolean equals(ListNode left, ListNode right) {
		if (left == null && right == null) {
			return true;
		}

		if (left != null) {
			return left.equals(right);
		}

		return false;
	}

}
