package utils;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class ArrayUtilsTest {

	@Test
	void shiftRight1() {
		final int[] nums = { 1, 2, 3, 4, 5 };
		final int[] expected = { 1, 2, 2, 3, 4 };

		ArrayUtils.shiftRight(nums, 1, 4);

		Assert.assertTrue(ArrayUtils.equals(expected, nums));
	}

	@Test
	void shiftRight2() {
		final int[] nums = { 1 };
		final int[] expected = { 1 };

		ArrayUtils.shiftRight(nums, 0, 0);

		Assert.assertTrue(ArrayUtils.equals(expected, nums));
	}

	@Test
	void shiftRight3() {
		final int[] nums = { 1, 2, 3, 4, 5 };
		final int[] expected = { 1, 2, 3, 4, 5 };

		ArrayUtils.shiftRight(nums, 4, 4);

		Assert.assertTrue(ArrayUtils.equals(expected, nums));
	}
}
