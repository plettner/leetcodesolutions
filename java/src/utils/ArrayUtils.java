package utils;

public class ArrayUtils {

	public static boolean equals(int[] expected, int[] actual) {
		if (expected.length != actual.length) {
			return false;
		}

		for (int index = 0; index < expected.length; index++) {
			if (expected[index] != actual[index]) {
				return false;
			}
		}

		return true;
	}

	public static boolean equalsFirstN(int[] expected, int[] actual, int length) {
		while (length > 0) {
			length--;
			if (expected[length] != actual[length]) {
				return false;
			}
		}
		return true;
	}

	public static int[] shiftRight(int[] array, int start, int length) {

		if (length > array.length) {
			throw new Error("Invalid parameters");
		}

		while (start < length) {
			array[length] = array[length - 1];
			length--;
		}
		return array;
	}

	public static String toString(int[] nums) {
		final StringBuilder sb = new StringBuilder();
		sb.append('[');
		if (nums.length > 0) {
			sb.append(nums[0]);
		}
		for (int index = 1; index < nums.length; index++) {
			sb.append(", ");
			sb.append(nums[index]);
		}
		sb.append(']');
		return sb.toString();
	}

}
