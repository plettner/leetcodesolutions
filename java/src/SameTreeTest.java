import org.junit.Assert;
import org.junit.jupiter.api.Test;

import utils.TreeNode;

class SameTreeTest {

	private final SameTree sameTree = new SameTree();

	@Test
	void example1() {
		final Integer[] array1 = { 1, 2, 3 };
		final Integer[] array2 = { 1, 2, 3 };
		final boolean expected = true;

		final TreeNode tree1 = TreeNode.createFromArray(array1);
		final TreeNode tree2 = TreeNode.createFromArray(array2);

		final boolean actual = this.sameTree.isSameTree(tree1, tree2);

		Assert.assertEquals(expected, actual);
	}

	@Test
	void example2() {
		final Integer[] array1 = { 1, 2 };
		final Integer[] array2 = { 1, null, 2 };
		final boolean expected = false;

		final TreeNode tree1 = TreeNode.createFromArray(array1);
		final TreeNode tree2 = TreeNode.createFromArray(array2);

		final boolean actual = this.sameTree.isSameTree(tree1, tree2);

		Assert.assertEquals(expected, actual);
	}

	@Test
	void example3() {
		final Integer[] array1 = { 1, 2, 1 };
		final Integer[] array2 = { 1, 1, 2 };
		final boolean expected = false;

		final TreeNode tree1 = TreeNode.createFromArray(array1);
		final TreeNode tree2 = TreeNode.createFromArray(array2);

		final boolean actual = this.sameTree.isSameTree(tree1, tree2);

		Assert.assertEquals(expected, actual);
	}

}
