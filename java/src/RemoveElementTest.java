import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import utils.ArrayUtils;

class RemoveElementTest {

	private final RemoveElement removeElement = new RemoveElement();

	@Test
	void example1() {
		final int[] actual = { 3, 2, 2, 3 };
		final int value = 3;
		final int[] expected = { 2, 2 };

		final int actualLength = this.removeElement.removeElement(actual, value);

		Assert.assertEquals(expected.length, actualLength);
		Assert.assertTrue(ArrayUtils.equalsFirstN(expected, actual, actualLength));
	}

	@Test
	void example2() {
		final int[] actual = { 0, 1, 2, 2, 3, 0, 4, 2 };
		final int value = 2;
		final int[] expected = { 0, 1, 4, 0, 3 };

		final int actualLength = this.removeElement.removeElement(actual, value);

		Assert.assertEquals(expected.length, actualLength);
		Assert.assertTrue(ArrayUtils.equalsFirstN(expected, actual, actualLength));
	}

	@Test
	@DisplayName("Short List, No Matches")
	void shortListNoMatches() {
		final int[] actual = { 0 };
		final int value = 2;
		final int[] expected = { 0 };

		final int actualLength = this.removeElement.removeElement(actual, value);

		Assert.assertEquals(expected.length, actualLength);
		Assert.assertTrue(ArrayUtils.equalsFirstN(expected, actual, actualLength));

	}

	@Test
	@DisplayName("Empty List, No Matches")
	void emptyListNoMatches() {
		final int[] actual = {};
		final int value = 2;
		final int[] expected = {};

		final int actualLength = this.removeElement.removeElement(actual, value);

		Assert.assertEquals(expected.length, actualLength);
		Assert.assertTrue(ArrayUtils.equalsFirstN(expected, actual, actualLength));
	}

	@Test
	@DisplayName("Short List, All Matches")
	void shortListAllMatches() {
		final int[] actual = { 2, 2 };
		final int value = 2;
		final int[] expected = {};

		final int actualLength = this.removeElement.removeElement(actual, value);

		Assert.assertEquals(expected.length, actualLength);
		Assert.assertTrue(ArrayUtils.equalsFirstN(expected, actual, actualLength));
	}

}
