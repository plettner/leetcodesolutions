
public class RomanToInteger {

	private static final char M = 'M';
	private static final char D = 'D';
	private static final char C = 'C';
	private static final char L = 'L';
	private static final char X = 'X';
	private static final char V = 'V';
	private static final char I = 'I';
	private static final char Z = '\0'; // represents an empty string

	private static final int MV = 1000;
	private static final int DV = 500;
	private static final int CV = 100;
	private static final int LV = 50;
	private static final int XV = 10;
	private static final int VV = 5;
	private static final int IV = 1;

	private char peek(String s) {
		if (s != null && s.length() > 0) {
			return s.charAt(0);
		}
		return Z;
	}

	private String dropFirstChar(String s) {
		if (s != null && s.length() > 1) {
			return s.substring(1);
		}
		return "";
	}

	public int convert(String roman) {
		return processThousands(roman);
	}

	int processThousands(String s) {
		int value = 0;
		while (peek(s) == M) {
			value += MV;
			s = dropFirstChar(s);
		}

		return value + processFiveHundreds(s);
	}

	int processFiveHundreds(String s) {
		int value = 0;

		if (peek(s) == Z) {
			return value;
		}

		if (peek(s) == D) {
			value = DV;
			s = dropFirstChar(s);
		}

		return value + processHundreds(s);
	}

	int processHundreds(String s) {
		int value = 0;

		if (peek(s) == Z) {
			return value;
		}

		if (peek(s) == C) {
			s = dropFirstChar(s);
			if (peek(s) == M) {
				s = dropFirstChar(s);
				value = MV - CV;
			} else if (peek(s) == D) {
				s = dropFirstChar(s);
				value = DV - CV;
			} else {
				value = CV;
				while (peek(s) == C) {
					s = dropFirstChar(s);
					value += CV;
				}
			}
		}
		return value + processFifties(s);
	}

	int processFifties(String s) {
		int value = 0;

		if (peek(s) == Z) {
			return value;
		}

		if (peek(s) == L) {
			value = LV;
			s = dropFirstChar(s);
		}

		return value + processTens(s);
	}

	int processTens(String s) {
		int value = 0;

		if (peek(s) == Z) {
			return value;
		}

		if (peek(s) == X) {
			s = dropFirstChar(s);
			if (peek(s) == C) {
				value = CV - XV;
				s = dropFirstChar(s);
			} else if (peek(s) == L) {
				value = LV - XV;
				s = dropFirstChar(s);
			} else {
				value = XV;
				while (peek(s) == X) {
					value += XV;
					s = dropFirstChar(s);
				}
			}
		}
		return value + processFives(s);
	}

	int processFives(String s) {
		int value = 0;

		if (peek(s) == Z) {
			return value;
		}

		if (peek(s) == V) {
			value = VV;
			s = dropFirstChar(s);
		}

		return value + processOnes(s);
	}

	int processOnes(String s) {
		int value = 0;

		if (peek(s) == Z) {
			return value;
		}

		if (peek(s) == I) {
			s = dropFirstChar(s);

			if (peek(s) == X) {
				value += XV - IV;
				s = dropFirstChar(s);
			} else if (peek(s) == V) {
				value += VV - IV;
				s = dropFirstChar(s);
			} else {
				value = IV;
				while (peek(s) == I) {
					value += IV;
					s = dropFirstChar(s);
				}
			}
		}

		if (s == null || s.length() > 0) {
			throw new Error("Possible invalid Roman Numeral; Here's what's left: " + s);
		}

		return value;
	}
}
