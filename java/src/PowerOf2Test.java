import org.junit.jupiter.api.Test;

class PowerOf2Test {

	private final PowerOf2 p2 = new PowerOf2();

	@Test
	void testABunch() {
		for (int index = 0; index < 257; index++) {
			if (this.p2.isPowerOf2(index)) {
				System.out.println(index);
			}
		}
	}

}
