import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class LongestCommonPrefixTest {

	private final LongestCommonPrefix longestcommonPrefix = new LongestCommonPrefix();

	@Test
	@DisplayName("Example #1")
	void example1() {
		final String[] strs = { "flower", "flow", "flight" };
		final String expected = "fl";
		final String actual = this.longestcommonPrefix.process(strs);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("All strings identical")
	void allStringsIdentical() {
		final String[] strs = { "flow", "flow", "flow" };
		final String expected = "flow";
		final String actual = this.longestcommonPrefix.process(strs);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("Empty String in the Mix")
	void emptyStringInArray() {
		final String[] strs = { "flower", "", "flow", "flight" };
		final String expected = "";
		final String actual = this.longestcommonPrefix.process(strs);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("No matching prefix")
	void noMatchingPrefix() {
		final String[] strs = { "aaa", "bbbbb", "cc", "dddd", "eeeee" };
		final String expected = "";
		final String actual = this.longestcommonPrefix.process(strs);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("No strings in array")
	void noStringsInArray() {
		final String[] strs = {};
		final String expected = "";
		final String actual = this.longestcommonPrefix.process(strs);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("Long Prefix")
	void longPrefix() {
		final String[] strs = { "aaaaaaaaa", "aaaaaaaaaaaaaaadfsd", "aaaaaadfsd", "aaaaaaaaa234fff", "aaaaaa" };
		final String expected = "aaaaaa";
		final String actual = this.longestcommonPrefix.process(strs);
		Assert.assertEquals(expected, actual);
	}

}
