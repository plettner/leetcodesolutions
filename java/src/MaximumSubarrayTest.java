import org.junit.Assert;
import org.junit.jupiter.api.Test;

class MaximumSubarrayTest {

	private final MaximumSubarray maxSum = new MaximumSubarray();

	@Test
	void example1() {
		final int[] nums = { -2, 1, -3, 4, -1, 2, 1, -5, 4 };
		final int expected = 6;

		final int actual = this.maxSum.maxSubArray(nums);

		Assert.assertEquals(expected, actual);
	}

	@Test
	void example2() {
		final int[] nums = { 1 };
		final int expected = 1;

		final int actual = this.maxSum.maxSubArray(nums);

		Assert.assertEquals(expected, actual);
	}

	@Test
	void example3() {
		final int[] nums = { 5, 4, -1, 7, 8 };
		final int expected = 23;

		final int actual = this.maxSum.maxSubArray(nums);

		Assert.assertEquals(expected, actual);
	}

}
