
public class RemoveElement {

	// The allow range is 0 <= nums[i] <= 50
	private static final int INVALID = -1;

	public int removeElement(int[] nums, int value) {

		int start = 0;
		int end = nums.length - 1;

		while (start < end) {
			if (nums[start] == value) {
				while (end > start && nums[end] == value) {
					nums[end] = INVALID;
					end--;
				}

				nums[start] = nums[end];
				nums[end--] = INVALID;
			}

			start++;
		}

		return end + 1;

	}

}
