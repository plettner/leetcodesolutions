import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ValidParenthesesTest {

	private final ValidParentheses validParens = new ValidParentheses();

	@Test
	void example1() {
		final boolean expected = true;
		final String value = "()";
		Assert.assertEquals(expected, this.validParens.isValid(value));
	}

	@Test
	void example2() {
		final boolean expected = true;
		final String value = "()[]{}";
		Assert.assertEquals(expected, this.validParens.isValid(value));
	}

	@Test
	void example3() {
		final boolean expected = false;
		final String value = "(]";
		Assert.assertEquals(expected, this.validParens.isValid(value));
	}

	@Test
	void example4() {
		final boolean expected = false;
		final String value = "([)]";
		Assert.assertEquals(expected, this.validParens.isValid(value));
	}

	@Test
	void example5() {
		final boolean expected = true;
		final String value = "{[]}";
		Assert.assertEquals(expected, this.validParens.isValid(value));
	}

	@Test
	@DisplayName("Now add some extra letters")
	void extraLetters90() {
		final boolean expected = true;
		final String value = "(This is a sentence (with parens) and it should work [Editor's Note:  It will.])";
		Assert.assertEquals(expected, this.validParens.isValid(value));
	}

}
