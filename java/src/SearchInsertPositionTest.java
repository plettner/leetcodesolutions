import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

class SearchInsertPositionTest {

	private final SearchInsertPosition sip = new SearchInsertPosition();

	@Test
	@DisplayName("example1")
	void example1(TestInfo testInfo) {
		final int[] nums = { 1, 3, 5, 6 };
		final int target = 5;
		final int expected = 2;

		final int actual = this.sip.searchInsert(nums, target);

		displayComparisonCount(testInfo);

		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("example2")
	void example2(TestInfo testInfo) {
		final int[] nums = { 1, 3, 5, 6 };
		final int target = 2;
		final int expected = 1;

		final int actual = this.sip.searchInsert(nums, target);

		displayComparisonCount(testInfo);

		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("example3")
	void example3(TestInfo testInfo) {
		final int[] nums = { 1, 3, 5, 6 };
		final int target = 7;
		final int expected = 4;

		final int actual = this.sip.searchInsert(nums, target);

		displayComparisonCount(testInfo);

		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("example4")
	void example4(TestInfo testInfo) {
		final int[] nums = { 1, 3, 5, 6 };
		final int target = 0;
		final int expected = 0;

		final int actual = this.sip.searchInsert(nums, target);

		displayComparisonCount(testInfo);

		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("example5")
	void example5(TestInfo testInfo) {
		final int[] nums = { 1 };
		final int target = 0;
		final int expected = 0;

		final int actual = this.sip.searchInsert(nums, target);

		displayComparisonCount(testInfo);

		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("Array of 1000")
	void arrayOf1000(TestInfo testInfo) {
		final int ARRAY_SIZE = 1000;
		final int[] nums = new int[ARRAY_SIZE];
		for (int index = 0; index < ARRAY_SIZE; index++) {
			nums[index] = index;
		}
		final int target = 999;
		final int expected = 999;

		final int actual = this.sip.searchInsert(nums, target);

		displayComparisonCount(testInfo);

		Assert.assertEquals(expected, actual);
	}

	private void displayComparisonCount(TestInfo testInfo) {
		// System.out.println(testInfo.getDisplayName() + " Comparison Count = " +
		// this.sip.getComparisonCount());
	}

}
