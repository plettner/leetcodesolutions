import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class RomanToIntegerTest {

	private final RomanToInteger romanToInteger = new RomanToInteger();
	private final IntToRoman intToRoman = new IntToRoman();

	@Test
	@DisplayName("Example 1: III")
	void example1() {
		final String value = "III";
		final int expected = 3;
		final int actual = this.romanToInteger.convert(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("Example 2: IV")
	void example2() {
		final String value = "IV";
		final int expected = 4;
		final int actual = this.romanToInteger.convert(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("Example 3: IX")
	void example3() {
		final String value = "IX";
		final int expected = 9;
		final int actual = this.romanToInteger.convert(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("Example 4: LVIII")
	void example4() {
		final String value = "LVIII";
		final int expected = 58;
		final int actual = this.romanToInteger.convert(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("Example 5: MCMXCIV")
	void example5() {
		final String value = "MCMXCIV";
		final int expected = 1994;
		final int actual = this.romanToInteger.convert(value);
		Assert.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("MMMDCCCLXXXVIII")
	void mmmdccclxxxviii() {
		final String value = "MMMDCCCLXXXVIII";
		final int expected = 3888;
		final int actual = this.romanToInteger.convert(value);
		Assert.assertEquals(expected, actual);
	}

	/**
	 * This test fails because it doesn't make sense to have CD follow CM and so on.
	 */
	@Test
	@DisplayName("CMCDXCXLIXIV")
	void cmcdxcxlixiv() {
		final String value = "CMCDXCXLIXIV";

		Assertions.assertThrows(Error.class, () -> {
			this.romanToInteger.convert(value);
		});
	}

	@Test
	@DisplayName("1 To 3999")
	void test1to3999() {
		for (int expected = 1; expected < 4000; expected++) {
			final String s = this.intToRoman.convert(expected);
			final int actual = this.romanToInteger.convert(s);
			Assert.assertEquals(expected, actual);
		}
	}
}
