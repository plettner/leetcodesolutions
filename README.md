# LeetCodeSolutions

Solutions to some of the tests at LeetCode.com.  I asked a prospective employer
recently how I might best prepare for my next interview with them, and they told
me to review the easy questions at LeetCode.  So I dug in and started implementing
solutions for some of those questions.  I sorted the questions difficulty, and 
went one by one.  Here's which questions I address with this code:

```
  1.  Two Sum
  7.  Reverse Integer
  9.  Palindrome Number
 13.  Roman to Integer
 14.  Longest Common Prefix
 20.  Valid Parentheses
 21.  Merge Two Sorted Lists
 26.  Remove Duplicates from Sorted Array
 27.  Remove Element
 28.  Implement strStr()
 35.  Search Insert Position
 53.  Maximum Subarray
 58.  Length of Last Word
 66.  Plus One
 67.  Add Binary
 69.  Sqrt(x)
 70.  Climbing Stairs
 83.  Remove Duplicates from Sorted List
 88.  Merge Sorted Array
 94.  Binary Tree Inorder Traversal
100.  Same Tree
169.  Majority Element
172.  Factorial Trailing Zeroes
206.  Reverse Linked List
219.  Contains Duplicate II
231.  Power of 2

 
```